
/// Test Formatter
import '../../../utilities/utilities/lib/formatter.dart';

void main() {
  Formatter fmt = new Formatter();
  //print(fmt);
  print('start:');
  fmt.writeLn('this is a test');
  fmt.indent();
  fmt.writeLn('a second line');
  fmt.indent();
  fmt.writeLn('number 3!');
  fmt.outdent();
  fmt.outdent();
  fmt.writeLn('4444444444444');
  //print('1: ${fmt.output}');
  //var s = fmt.lineNo;
  //print('2: $s');
  //var t = fmt.spaces;
  //print('3: $t');
  //print('middle');
  fmt.flush();
  print('end');
}