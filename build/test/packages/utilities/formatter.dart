// Open DICOMweb Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library formatter;

import 'byte_array.dart';

/**
 * A utility class for creating nested indentation.  The [indenter] has an internal count
 * of the current indentation level (number of spaces to indent).
 *
 * For example, given an [indenter] named *indent* with an internal count of four, the String
 * '$indent some text' will return the following [String] '     some text'.
 */
class Formatter {
  List<String> output = new List();
  //ByteArray buf = new ByteArray(4096);
  bool hasLineNumbers = true;
  bool hasPrintLevel = true;
  bool isIndented = true;
  int _lineNo = 0;
  int _level = 0;
  int _indent =0;
  int _increment = 2;

  Indenter({int lineNo: 0, int level: 0, int indent: 0, int increment: 2}) {
    if (lineNo < 0) hasLineNumbers = false;
    if (level < 0)  hasPrintLevel = false;
    if (indent < 0) isIndented = false;
    _lineNo = lineNo;
    _level = level;
    _indent = indent;
    _increment = increment;
  }

  /// Increments the indentation level by i spaces.
  int operator + (int count) => _indent += count;

  /// Decrements the indentation level by [count] spaces.
  int operator - (int count) => _indent -= count;

  /// The current indention count
  int get count => _indent;

  //TODO are there better names than [inc] and [dec]?
  /// Increments the indentation level by [_increment] spaces.
  void indent([int n = 1]) {
    _level = _level + n;
    _indent = _indent + (n * _increment);
    // print('_level=$_level, _indent=$_indent');
  }

  /// Decrements the indentation level by [_increment] spaces.
  int outdent([int n = 1]) {
    _level = _level - n;
    _indent = _indent - (n * _increment);
    return _indent;
  }

  //TODO Determine which name is better [indent] or [spaces] or [sp]
  /// Returns a [String] of spaces the length of the current indentation level
  //String get indent => "".padRight(_indent);  // Inserts spaces
  String get spaces => "".padRight(_indent);    // Inserts spaces
  //String get sp => indent;                    // Inserts spaces
  String get lineNo {
    var s = _lineNo.toString();
    s = s.padLeft(4);
    return s += ": ";
    }

  String get prefix => lineNo + spaces;

  /// Resets the [indenter] [level to 0 and the [indent] to 0.
  //TODO: this could remember the initial values and reset to them.  Implement
  //      if needed.
  int get reset { _level = 0; return _indent = 0; } // Resets indent to [start]

  void write(String s) { output.add(s); }

  void writeNewline() => output.add("\n");

  void writeLn(String s) {
    //print(s);
    //print('lineNo=$lineNo');
    //print('spaces="$spaces"');
    //print('prefix=$prefix');
    //print('_lineNo=$_lineNo, _indent=$_indent');
    var s1 = lineNo + spaces + s;
    write(s1);
    writeNewline();
    _lineNo++;
    //print('output=$output');
    //print('writeLn: $s\n');

  }

  void writeLnIndented(String s) {
    indent();
    writeLn(s);
    outdent();
  }

  void flush() {
    print(output.join());
  }

  toString() => 'Formatter:[$_lineNo, $_level, $_indent, $_increment]';

}