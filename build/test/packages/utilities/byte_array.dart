// Open DICOMweb Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library byte_array;

import 'dart:typed_data';
import 'package:charcode/ascii.dart';

/// A library for parsing [Uint8List], aka [ByteArray]
///
/// Supports parsing both BIG_ENDIAN and LITTLE_ENDIAN byte arrays. The default
/// Endianness is the endianness of the host [this] is running on, aka HOST_ENDIAN.
/// All read* methods advance the [position] by the number of bytes read.

class ByteArray {
  Uint8List uint8List;
  ByteData bd;
  Endianness endianness = Endianness.HOST_ENDIAN;
  int start;
  int position;
  int limit;

  ByteArray(int length, [this.endianness]) {
    uint8List = new Uint8List(length);
    start = 0;
    init(length, endianness);
  }

  ByteArray.fromBuffer(ByteBuffer buffer, [this.start = 0, length, this.endianness]) {
    this.uint8List = buffer.asUint8List(start, length);
    init(length, endianness);
  }

  ByteArray.fromUint8List(this.uint8List, [this.start = 0, length, this.endianness]) {
    init(length, endianness);
  }

  ByteArray.view(ByteArray buf, [this.start = 0, length]) {
    uint8List = buf.uint8List;
    init(length, buf.endianness);
  }

  void init(int length, Endianness endianness) {
    bd = uint8List.buffer.asByteData();
    if (endianness == null) this.endianness = Endianness.HOST_ENDIAN;
    int end = uint8List.lengthInBytes;
    limit = (length == null) ? end : start + length;
    if (start <   0)
      throw "Start= $start - cannot be less than zero";
    if (limit > end)
      throw "Limit= $limit - cannot be greater than buffer length =$end";
    position = start;
  }

  //TODO remove this.
  /*
  static ByteArray subArray(int start, int limit) {
    return new ByteArray.view(this, start, limit);
  }
  */
  int operator [] (int i) => uint8List[i];
  void operator []= (int i, int val) { uint8List[i] = val; }

  int  get length => limit - position;
  bool get isEmpty => position >= limit;
  bool get isNotEmpty => !isEmpty;

  int seek(int n) {
    checkRange(position, n, 1, "seek");
    return position += n;
  }

  int getLimit(int lengthInBytes) {
    int localLimit = position + lengthInBytes;
    if (localLimit > limit) throw "length $lengthInBytes too long";
    return localLimit;
  }

  int getLength(int offsetInBytes) {
    if (offsetInBytes > limit) throw "Offset $offsetInBytes too long";
    return position - offsetInBytes;
  }

  int checkLimit(int maxPosition) {
    if (maxPosition >= limit) {
      //TODO add to Warnings
      return limit;
    } else {
      return maxPosition;
    }
  }

  checkRange(int offset, int length, int sizeInBytes, String caller) {
    int position = offset + (length * sizeInBytes);
    if (position < start) {
      throw '$caller: position cannot be less than 0';
    }
    if (position > limit) {
      throw '$caller: attempt to read past end of buffer';
    }
  }

  /// [offset] is an absolute offset in the [uint8List]. Returns an unsigned 8 bit integer.
  int getUint8(int offset) {
    checkRange(offset, 1, 1, "Uint8");
    return bd.getUint8(offset);
  }

  /// Reads an unsigned 8 bit integer from the current [position] in [uint8List] and
  /// increments the [position]  by 1. Throws an error if [position] is out of range.
  int readUint8() {
    int value = getUint8(position);
    position += 1;
    return value;
  }

  /// [offset] is an absolute offset in the [uint8List]. Returns an unsigned 8 bit integer.
  /// Throws an error if [position] is out of reange.
  int getInt8(int offset) {
    checkRange(offset, 1, 1, "Int8");
    return bd.getInt8(offset);
  }

  /// Returns an unsigned 16 bit integer from [this] or throws and error.
  int readInt8() {
    int value = getInt8(position);
    position += 1;
    return value;
  }

  /// Gets 16 bits at the absolute [offset] in the [uint8List]. Returns the unsigned 16
  ///  bit value as an [int].  Throws an error if [position] is out of reange.
  int getUint16(int offset) {
    checkRange(offset, 1, 2, "Uint16");
    return bd.getUint16(offset, endianness);
  }

  /// Reads the 16 bits at the current [position] as an unsigned integer, increments
  /// the position by 2, and returns an [int].  Throws an error if [position] is out
  /// of range.
  int readUint16() {
    int value = getUint16(position);
    position += 2;
    return value;
  }

  /// Gets 16 bits at the absolute [offset] in the [uint8List]. Returns the unsigned 16
  ///  bit value as an [int].  Throws an error if [position] is out of reange.
  int getInt16(int offset) {
    checkRange(offset, 1, 2, "Int16");
    return bd.getInt16(offset, endianness);
  }

  /// Reads a 16 bit signed integer int 16 from the byte array.
  int readInt16() {
    int value = getInt16(position);
    position += 2;
    return value;
  }

  int getUint32(int offset) {
    checkRange(offset, 1, 4, "Uint32");
    return bd.getUint32(offset, endianness);
  }

  /// Reads a 32 bit unsigned integer from the byte array.
  int readUint32() {
    int value = getUint32(position);
    position += 4;
    return value;
  }

  int getInt32(int offset) {
    checkRange(offset, 1, 4, "Int32");
    return bd.getInt32(offset, endianness);
  }

  /// Reads a 32 bit signed integer from the byte array.
  int readInt32() {
    int value = getInt32(position);
    position += 4;
    return value;
  }

  int getUint64(int offset) {
    checkRange(offset, 1, 8, "Uint64");
    return bd.getUint64(offset, endianness);
  }

  /// Reads a 32 bit unsigned integer from the byte array.
  int readUint64() {
    int value = getUint64(position);
    position += 8;
    return value;
  }

  int getInt64(int offset) {
    checkRange(offset, 1, 8, "Int64");
    return bd.getInt64(offset, endianness);
  }

  /// Reads a 32 bit signed integer from the byte array.
  int readInt64() {
    int value = getInt64(position);
    position += 8;
    return value;
  }

  double getFloat32(int offset) {
    checkRange(offset, 1, 4, "Float32");
    return bd.getFloat32(offset, endianness);
  }

  /// Synonym for getFloat32
  double getFLoat(int offset) => getFloat32(offset);

  /// Reads a 32 bit floating point number from the byte array.
  double readFloat32() {
    double value = getFloat32(position);
    position += 4;
    return value;
  }

  /// Synonym for readFloat32
  double readFloat() => readFloat32();

  double getFloat64(int offset) {
    checkRange(offset, 1, 8, "Float64");
    return bd.getFloat64(offset, endianness);
  }

  /// Reads a 64 bit floating point number from the byte array.
  double readFloat64() {
    double value = getFloat64(position);
    position += 8;
    return value;
  }

  /// Synonym for readFloat64
  double readDouble() => readFloat64();

  //TODO isn't there a faster way to do this?
  //TODO should string trimming be done here?
  String getFixedString(int offset, int length) {
    checkRange(offset, length, 1, "FixedString");
    var result = "";
    for (var i = offset; i < offset + length; i++) {
      var byte = uint8List[i];
      if ((byte == 0) || (byte == $backslash)) {
        Uint8List charCodes = uint8List.buffer.asUint8List(offset, i - offset);
        var s = new String.fromCharCodes(charCodes);
        //print('getFixedString1:"$s"');
        return s.trimRight();
      }
    }
    Uint8List charCodes = uint8List.buffer.asUint8List(offset, length);
    var s = new String.fromCharCodes(charCodes);
    //print('getFixedString2:"$s"');
    return s.trimRight();
  }

  /// Reads a string of 8 bit characters from the byte array.
  String readFixedString(int length) {
    var s = getFixedString(position, length);
    position += length;
    return s;
  }

}
