// Open DICOMweb Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library indenter;
**** NOT WORKING CODE
/**
 * A utility class for creating nested indentation.  The [indenter] has an internal count
 * of the current indentation level (number of spaces to indent).
 *
 * For example, given an [indenter] named *indent* with an internal count of four, the String
 * '$indent some text' will return the following [String] '     some text'.
 */
class Indenter {
  int _lineNo = 0;
  int _level = 0;
  int _indent;
  int _increment;

  Indenter({lineNo: 0, level: 0, indent: 0, increment: 2});

  /// Increments the indentation level by i spaces.
  int operator + (int count) => _indent += count;

  /// Decrements the indentation level by [count] spaces.
  int operator - (int count) => _indent -= count;

  /// The current indention count
  int get count => _indent;

  //TODO are there better names than [inc] and [dec]?
  /// Increments the indentation level by [_increment] spaces.
  int get inc {
    _level++;
    return _indent = _increment;
  }

  /// Decrements the indentation level by [_increment] spaces.
  int get dec {
    _level--;
    return _indent -= _increment;
  }

  //TODO Determine which name is better [indent] or [spaces] or [sp]
  /// Returns a [String] of spaces the length of the current indentation level
  String get indent => "".padRight(_indent);  // Inserts spaces
  String get spaces => indent;                // Inserts spaces
  String get sp => indent;                    // Inserts spaces
  String get lineNo => lineNo.toString().padLeft(4);
  String get prefix => lineNo + indent;

  /// Resets the [indenter] [level to 0 and the [indent] to 0.
  //TODO: this could remember the initial values and reset to them.  Implement
  //      if needed.
  int get reset { _level = 0; return _indent = 0; } // Resets indent to [start]

  void write(String s) {

  }

  void writeLn(String s) {

  }

  toString() => indent;

}