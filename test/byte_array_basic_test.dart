
library test.byte_array_basic_test;

import 'dart:typed_data';
import 'package:unittest/unittest.dart';

import '../../../utilities/utilities/lib/byte_array.dart';

void byteArrayBasicTest() {

  group("Basic ByteArray Tests", (){

    test("construction", () {
        // Arrange
        var buf = new ByteArray(32);

        // Assert
        expect(buf, isNotNull, reason: "construction did not return object");
    });

    test("position 0 on creation", () {
        // Arrange
        var buf = new ByteArray(32);

        // Assert
        expect(buf.position, 0, reason: "position 0");
    });

    test("position 10 on creation", () {
        // Arrange
        var bytes = new Uint8List(32);

        // Act
        var buf = new ByteArray.fromUint8List(bytes, 10);

        // Assert
        expect(buf.position, 10, reason: "position 10");
    });

    /*
    test("missing constructor parameter throws", () {
        // Arrange

       var t1 = (){ new ByteArray.fromUint8List(); };
        // Act
        expect((t1), throws,
            reason: "construction without byteArray parameter throws");
    });
    */

    /*
    test("constructor throws if byteArray parameter is not Uint8List", () {
        // Arrange
        var uint16List = new Uint16List(32);

        // Act
        expect(new ByteArray.fromUint8List(uint16List),
               throws,
               reason: "construction did not throw on invalid type for byteArray parameter");
    });
    */

    //TODO how to test Throws
    /*
    test("position cannot be < 0", () {
        // Arrange
        var bytes = new Uint8List(32);

        // Act
        expect(new ByteArray.fromUint8List(bytes, -1), throws,
                reason: "position cannot be < 0");
    });

    test("position cannot equal or exceed array length", () {
        // Arrange
        var bytes = new Uint8List(32);

        // Act
        expect(new ByteArray.fromUint8List(bytes, 32),
               throwsA(new isInstanceOf<ByteArray>()),
               reason: "position cannot exceed array length");
    });
    */
    test("seek succeeds", () {
        // Arrange
        var buf = new ByteArray(32);

        // Act
        buf.seek(10);

        // Assert
        expect(buf.position, 10, reason: "position 10");
    });

    /*
    test("seek to negative position throws", () {
        // Arrange
        var buf = new ByteArray(32);
        var func = (){buf.seek(-1); };

        // Act
        expect(func, throws, reason: "seek to negative position not throw");
    });
    */
    test("readByteStream returns object", () {
        // Arrange
        var buf = new ByteArray(32);

        // Act
        var subArray = new ByteArray.view(buf, 0, 5);

        // Assert
        expect(subArray, new isInstanceOf<ByteArray>(),
               reason: "readByteStream did not return an object");
    });

    test("readByteStream returns array with size matching numBytes parameter", () {
        // Arrange
        var buf = new ByteArray(32);

        // Act
        var subArray = new ByteArray.view(buf, 0, 5);

        // Assert
        expect(subArray.length, 5, reason: "readByteStream returned object with byteArray of wrong length");
    });

    test("readByteStream returns object with position 0", () {
        // Arrange
        var buf = new ByteArray(32);

        // Act
        var subArray = new ByteArray.view(buf, 0, 5);

        // Assert
        expect(subArray.position, 0, reason: "readByteStream returned object with position not 0");
    });

    test("readByteStream can read all remaining bytes", () {
        // Arrange
        var buf = new ByteArray(32);

        // Act
        var subArray = new ByteArray.view(buf, 0, 5);

        // Assert
        expect(subArray, new isInstanceOf<ByteArray>(),
               reason: "readByteStream did not return an object");
    });
    /*
    test("readByteStream throws on buffer overread", () {
        // Arrange
        var buf = new ByteArray(32);

        // Act
        expect(buf.getUint8(40), throws,
               reason: "readByteStream did not throw on buffer overread");
    });
    */
    test("readUint16 works", () {
        // Arrange
        var bytes = new Uint8List(32);
        bytes[0] = 0xff;
        bytes[1] = 0x80;
        var buf = new ByteArray.fromUint8List(bytes);

        // Act
        var uint16 = buf.readUint16();

        // Assert
        expect(uint16, 0x80FF, reason: "readUint16 did not return expected value = ${0x80ff}");
    });

    test("readUint16 can read at end of buffer", () {
        // Arrange
        var bytes = new Uint8List(2);
        bytes[0] = 0xff;
        bytes[1] = 0x80;
        var buf = new ByteArray.fromUint8List(bytes);

        // Act
        var uint16 = buf.readUint16();

        // Assert
        expect(uint16, 0x80ff, reason: "readUint16 did not return expected value");
    });

    /*
    test("readUint16 throws on buffer overread", () {
        // Arrange
        var buf = new ByteArray(32);

        buf.seek(31);
        // Act
        expect(buf.readUint16(), throws,
               reason: "readUint16 did not throw on buffer overread");
    });
    */

    test("readUint32 works", () {
        // Arrange
        var bytes = new Uint8List(32);
        bytes[0] = 0x11;
        bytes[1] = 0x22;
        bytes[2] = 0x33;
        bytes[3] = 0x44;
        var buf = new ByteArray.fromUint8List(bytes);

        // Act
        var uint32 = buf.readUint32();

        // Assert
        expect(uint32, 0x44332211, reason: "readUint32 did not return expected value");
    });

    test("readUint32 can read at end of buffer", () {
        // Arrange
        var bytes = new Uint8List(4);
        bytes[0] = 0x11;
        bytes[1] = 0x22;
        bytes[2] = 0x33;
        bytes[3] = 0x44;
        var buf = new ByteArray.fromUint8List(bytes);

        // Act
        var uint32 = buf.readUint32();

        // Assert
        expect(uint32, 0x44332211, reason: "readUint32 did not return expected value");
    });

    /*
    test("readUint32 throws on buffer overread", () {
        // Arrange
        var buf = new ByteArray(32);
        buf.seek(31);

        // Act
        expect(buf.readUint32(), throws,
               reason: "readUint32 did not throw on buffer overread");
    });
    */

    test("readFixedString works", () {
        // Arrange
        var bytes = new Uint8List(32);
        var str = "Hello";
        for(var i=0; i < str.length; i++) {
            bytes[i] = str.codeUnitAt(i);
        }
        var buf = new ByteArray.fromUint8List(bytes);

        // Act
        var fixedString = buf.readFixedString(5);

        // Assert
        expect(fixedString, 'Hello', reason: "readFixedString did not return expected value");
    });
    /*
    test("readUint32 throws on buffer overread", () {
        // Arrange
        var buf = new ByteArray(32);

        // Act
        expect(buf.readFixedString(33), throws,
               reason: "readFixedString did not throw on buffer overread");
    });

    test("readUint32 throws on negative length", () {
        // Arrange
        var buf = new ByteArray(32);

        // Act
        expect(buf.readFixedString(-1), throws,
               reason: "readFixedString did not throw on negative length");
    });
    */
    test("readFixedString can read at end of buffer", () {
        // Arrange
        var bytes = new Uint8List(5);
        var str = "Hello";
        for(var i=0; i < str.length; i++) {
            bytes[i] = str.codeUnitAt(i);
        }
        var buf = new ByteArray.fromUint8List(bytes);

        // Act
        var fixedString = buf.readFixedString(5);

        // Assert
        expect(fixedString, "Hello", reason: "readFixedString did not return expected value");
    });

    test("readFixedString can read null terminated string", () {
        // Arrange
        var bytes = new Uint8List(6);
        var str = "Hello";
        for(var i=0; i < str.length; i++) {
            bytes[i] = str.codeUnitAt(i);
        }
        var buf = new ByteArray.fromUint8List(bytes);

        // Act
        var fixedString = buf.readFixedString(6);

        // Assert
        expect(fixedString, "Hello", reason: "readFixedString did not return expected value");
    });

    test("readFixedString sets position properly after reading null terminated string", () {
        // Arrange
        var bytes = new Uint8List(6);
        var str = "Hello";
        for(var i=0; i < str.length; i++) {
            bytes[i] = str.codeUnitAt(i);
        }
        var buf = new ByteArray.fromUint8List(bytes);

        // Act
        var fixedString = buf.readFixedString(6);

        // Assert
        expect(buf.position, 6, reason: "readFixedString did not set position propery after reading null terminated string");
    });

  });
}