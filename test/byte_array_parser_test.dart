
library test.byte_array_parser_test;

import 'package:unittest/unittest.dart';
//import '../lib/dataset.dart';
import '../../../utilities/utilities/lib/byte_array.dart';

void byteArrayParserTest() {

  group("ByteArray Parser Tests", () {

    test("getUint16", () {
        // Arrange
        var buf = new ByteArray(32);
        buf[0] = 0xff;
        buf[1] = 0x80;

        // Act
        int uint16 = buf.getUint16(0);

        // Assert
        expect(uint16, 0x80ff, reason: "getUint16 returned $uint16 expected 0x80ff");
    });
    /*
    test("getUint16 throws on buffer overread", () {
        // Arrange
        var buf = new ByteArray(32);

        // Act
        expect(buf.getUint16(31), throws);
              // reason: "getUint16 did not throw on buffer overread");
    });

    test("getUint16 throws on position < 0", () {
        // Arrange
        var buf = new ByteArray(32);

        // Act
        expect(buf.getUint16(-1), throws);
        //        reason: "getUint16 did not throw on buffer overread");
    });
    */
    test("getUint32", () {
        // Arrange
        var buf = new ByteArray(32);
        buf[0] = 0x11;
        buf[1] = 0x22;
        buf[2] = 0x33;
        buf[3] = 0x44;

        // Act
        var uint32 = buf.getUint32(0);

        // Assert
        expect(uint32, 0x44332211, reason: "getUint32 did not return expected value");
    });

    test("getUint32", () {
        // Arrange
        var buf = new ByteArray(4);
        buf[0] = 0xFF;
        buf[1] = 0xFF;
        buf[2] = 0xFF;
        buf[3] = 0xFF;

        // Act
        var uint32 = buf.getUint32(0);

        // Assert
        expect(uint32, 4294967295, reason: "getUint32 did not return expected value");
    });

    /*
    test("getUint32 throws on buffer overread", () {
        // Arrange
        var buf = new ByteArray(32);

        // Act
        expect(() { var uint32 = buf.getUint32(30); },
               throwsA(new isInstanceOf<String>()),
               reason: "getUint32 did not throw on buffer overread");
    });

    test("getUint32 throws on position < 0", () {
        // Arrange
        var buf = new ByteArray(32);

        // Act
        expect(buf.getUint32(-1), throws);
        //       reason: "getUint32 did not throw on buffer overread");
    });
    */
    test("getFixedString can read at end of buffer",
         () {
          // Arrange
          var buf = new ByteArray(5);
          var str = "Hello";
          for(var i=0; i < str.length; i++) {
              buf[i] = str.codeUnitAt(i);
          }
          // Act
          var fixedString = buf.getFixedString(0, 5);
          // Assert
          expect(fixedString, equals("Hello"));
          //       reason: "getFixedString did not return expected value");
        });

    test("readFixedString can read null terminated string", () {
        // Arrange
        var buf = new ByteArray(6);
        var str = "Hello";
        for(var i=0; i < str.length; i++) {
            buf[i] = str.codeUnitAt(i);
        }

        // Act
        var fixedString = buf.getFixedString(0, 6);

        // Assert
        expect(fixedString,
               equals("Hello"),
               reason: "readFixedString did not return expected value");
    });

    test("readInt16 can read null terminated string", () {
        // Arrange
        var buf = new ByteArray(6);
        buf[0] = 0xFF; // -1
        buf[1] = 0xFF;

        // Act
        var int16 = buf.getInt16(0);

        // Assert
        expect(int16, -1, reason: "readInt16 did not return expected value");
    });

    test("readInt32 can read null terminated string", () {
        // Arrange
        var buf = new ByteArray(6);
        buf[0] = 0xFF; // -1
        buf[1] = 0xFF;
        buf[2] = 0xFF;
        buf[3] = 0xFF;

        // Act
        var int32 = buf.getInt32(0);

        // Assert
        expect(int32, -1, reason: "readInt32 did not return expected value");
    });

    test("readFloat works", () {
        // Arrange
        var buf = new ByteArray(5);
        buf[0] = 0x00;
        buf[1] = 0x00;
        buf[2] = 0xB4;
        buf[3] = 0xC0;

        // Act
        var float32 = buf.getFloat32(0);

        // Assert
        expect(float32, -5.625000, reason: "readFloat did not return expected value");
    });

    //commented out since qunit doesn't seem to have Float64Array type - works ok in chrome though
    test("readDouble works", () {
        // Arrange
        var buf = new ByteArray(8);
        buf[7] = 0x7f;
        buf[6] = 0xef;
        buf[5] = 0xff;
        buf[4] = 0xff;
        buf[3] = 0xff;
        buf[2] = 0xff;
        buf[1] = 0xff;
        buf[0] = 0xff;

        // Act
        var doubleValue = buf.getFloat64(0);

        // Assert
        expect(doubleValue,
              1.7976931348623157e+308,
              reason: "readDouble did not return expected value");
    });

  });

}
